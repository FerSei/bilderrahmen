package com.sabel.bilderrahmen;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.SwingUtilities;

import com.sabel.bilderrahmen.Mail.EmailEntscheiden;
import com.sabel.bilderrahmen.db.ConfigService;
import com.sabel.bilderrahmen.view.Controller;
import com.sabel.bilderrahmen.view.Fenster;

/**
 * Mails nach Muster: anzahlbilder, anzeigedauer in sekunden , ladezeit in
 * minuten(nicht unbedingt notwendig) mindestens 3 Bilder, hoechstens aller 3
 * sekunden, ladezeit min 5 minuten
 * 
 * @author Ferdiannd Seibold, Alexander Brunzel
 *
 */

public class Main {
	/**
	 * Timer zum Pruefen nach einer neuen Mail mit neuen Bildern oder Config,
	 * Intervall ist die ladezeit
	 */
	public static void starteTimer() {
		ConfigService configservice = new ConfigService();
		int ladezeit = configservice.gibLetzteConfig().getLadezeit();

		Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				try {
					/**
					 * prueft und holt sich ggf. neue Mails und importiert ggf.
					 * neue Bilder und/oder Config
					 */
					new EmailEntscheiden();

					/**
					 * wenn neue Ladezeit in der aktuellsten Config steht, wird
					 * neu gestartet
					 */
					ConfigService configservice = new ConfigService();
					int ladezeit = configservice.gibLetzteConfig().getLadezeit();
					int ladezeitNeu = configservice.gibLetzteConfig().getLadezeit();

					if (ladezeit != ladezeitNeu) {
						cancel();
						starteTimer();
					}

					/**
					 * Bilderlauf wird gestartet
					 */
					Controller controller = new Controller();
					Fenster fenster = new Fenster(controller);

				} catch (Exception e) {
					e.printStackTrace();
					System.exit(-1);
				}
			}
		}, 0, ladezeit);
	}

	public static void main(String[] args) {
		String name = "pi1";
		if (args.length > 0) {
			name = args[0];
		}
		System.out.println(name);
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				Controller controller = new Controller();
				try {
					controller.skaliereAlleBilder(controller.loescheSkalierteBilder());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				starteTimer();
			}
		});
	}
}

/*
 * Programmtest: (++ erfolgreich)
 * 
 * ein neues Bild ++
 * 
 * 2 neue Bildern ++
 * 
 * eine config ++
 * 
 * eine neue config ++
 *
 * eine falsche config ++
 * 
 * eine config mit zu riesigen zahlen ++
 * 
 * eine falsche config mit Bild ++
 * 
 * eine config mit Bild ++
 * 
 * Spam mit config beginn ++
 * 
 * config mit * ++
 */
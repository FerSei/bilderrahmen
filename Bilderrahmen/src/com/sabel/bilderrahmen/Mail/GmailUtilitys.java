package com.sabel.bilderrahmen.Mail;

import java.util.Properties;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;

import com.sabel.bilderrahmen.db.EmailService;
import com.sun.mail.pop3.POP3SSLStore;

/**
 * 
 * @author Alexander Brunzel
 *
 */

public class GmailUtilitys {
	private Session session;
	private Store store;
	private Folder folder;

	// Getestet und funktionsf�hig
	public GmailUtilitys() {
	}
	
	public Folder getFolder(){
		return folder;
	}
	// Anmdeldung an GMail Konto I4aProjekt
	public void verbinde() throws Exception {
		EmailService es = new EmailService();
		
		String login = es.gibLetzteEmail().getEmailAdresse();
		String passwort = es.gibLetzteEmail().getPasswort();

		String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";

		Properties pop3Props = new Properties();

		pop3Props.setProperty("mail.pop3.socketFactory.class", SSL_FACTORY);
		pop3Props.setProperty("mail.pop3.socketFactory.fallback", "false");
		pop3Props.setProperty("mail.pop3.port", "995");
		pop3Props.setProperty("mail.pop3.socketFactory.port", "995");

		URLName url = new URLName("pop3", "pop.gmail.com", 995, "", login, passwort);

		session = Session.getInstance(pop3Props, null);
		store = new POP3SSLStore(session, url);
		store.connect();
	}

	// Den Ordner oeffnen
	public void Ordneroeffnen(String folderName) throws Exception {

		folder = store.getDefaultFolder();
		folder = folder.getFolder(folderName);

		if (folder == null) {
			throw new Exception("Falscher Ordner");
		}
		// Den ordner oeffnen
		try {
			folder.open(Folder.READ_WRITE);

		} catch (MessagingException ex) {
			folder.open(Folder.READ_ONLY);

		}
	}

	// Ordner Schliessen
	public void schliesseOrdner() throws Exception {
		folder.close(false);
	}

	// Anzahl der Mails
	public int getNachrichtenAnzahl() throws Exception {
		return folder.getMessageCount();

	}

	// Verbindung schliessen
	public void trenneVerbindung() throws Exception {
		store.close();
	}

}
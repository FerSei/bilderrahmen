package com.sabel.bilderrahmen.Mail;

import java.util.List;

import javax.mail.Message;

import com.sabel.bilderrahmen.db.Bild;
import com.sabel.bilderrahmen.db.BilderService;
import com.sabel.bilderrahmen.db.ConfigService;

/**
 * 
 * @author Alexander Brunzel
 *
 */

// TODO Problemklasse
public class EmailEntscheiden {

	GmailUtilitys gmail = new GmailUtilitys();
	EmailAuswerten email = new EmailAuswerten();

	public EmailEntscheiden() throws Exception {
		// wie viele nachrichten sollen ausgewertet werden
		nachrichtenVerteiler();
	}

	// funktioniert
	public void nachrichtenVerteiler() throws Exception {
		// einloggen in email
		gmail.verbinde();
		gmail.Ordneroeffnen("INBOX");

		int startNachricht = 0;
		int gesammtNachrichten = gmail.getNachrichtenAnzahl();
		// TODO testnachricht
		System.out.println("Gesammtnachrichten: " + gesammtNachrichten);
		// anzahl der zu auzuw�hlenden mails kann nicht negativ sein
		int anzahl = 20;
		if ((gesammtNachrichten - anzahl) >= 0) {
			// viele emails im postfach lese nur "anzahl" mails
			startNachricht = gesammtNachrichten - anzahl + 1;
		} else {
			// nicht genug emails im postfach checke alle
			startNachricht = 1;
		}
		// TODO testnachricht
		System.out.println("Startnachricht " + startNachricht);
		for (int i = startNachricht; i <= gesammtNachrichten; i++) {
			Message mss = gmail.getFolder().getMessage(i);
			System.out.println("--------------------------");
			System.out.println("Nachricht #" + (i) + ":");
			// bekommt false wenn irrelevant
			if (chooseMessage(mss)) {
				// TODO testausgabe
				System.out.println("Elemente werden Verarbeitet");
				System.out.println("weiter mit inhaltZerteilen");
				// config? Bild? Default?
				email.inhaltZerteilen(mss);
			} else {
				// TODO testelse
				System.out.println("Nachricht Irrelevant");
			}
		}
	}

	// Soll die fuer den Betreffenden Rasperie PI die Nachrichten Auswaehlen
	// Geraetenamen mit Betreff vergleichen ausser bei alle
	private boolean chooseMessage(Message mss) throws Exception {
		ConfigService cs = new ConfigService();
		BilderService bs = new BilderService();
		// liefert das datum des letzten configeintrags zur�ck
		long confDatum = Long.parseLong(cs.gibLetzteConfig().getDatum());
		String geraeteName = cs.gibLetzteConfig().getGeraetename();
		// Betreffvergleich
		// hab den filter ausgeschaltet das ich alle emails auswerten

		if (((mss.getSubject().equalsIgnoreCase("alle") || mss.getSubject().equalsIgnoreCase(
				geraeteName)))  && mss.isMimeType("multipart/*") ) {
			// TODO Testausgabe
			System.out.println("Email ist f�r mich");

			// configvergleich
			if (configvergleich(confDatum, mss)) {
				// TODO Testausgabe
				System.out.println("eintrag bereits in config DB");
				return false;
			}
			// bildervergleich
			List<Bild> bildListe = bs.gibAlleBilder();
			for (int i = 0; i < bildListe.size(); i++) {
				long bilderDatum = Long.parseLong(bildListe.get(i).getDatum());
				if (bildervergleich(bilderDatum, mss)) {
					// TODO Testausgabe
					System.out.println("eintrag bereits in bilder DB");
					return false;
				}
			}
			// TODO testausgabe
			System.out.println("weder bild noch config in DB ist aber f�r mich");
			return true;
		}
		// TODO testausgabe
		System.out.println("Email nicht f�r mich");
		return false;
	}

	// Alle bilder bekommen und vergleicht ob die zeit aus email mit einem aus
	// der db �bereinstimmt true = bild behandelt
	private boolean bildervergleich(long bilderDatum, Message mss) throws Exception {
		long emailDatum = mss.getSentDate().getTime();
		if (emailDatum == bilderDatum) {
			// TODO Testausgabe
			System.out.println("datum aus mail   " + emailDatum);
			System.out.println("datum aus bilddb " + bilderDatum);
			return true;
		}
		return false;
	}

	// letzte Config bekommen und vergleicht ob die zeit aus der email gr��er
	// ist letzen aus db �bereinstimmt true = config behandelt
	private boolean configvergleich(long confDatum, Message mss) throws Exception {
		long emailDatum = mss.getSentDate().getTime();
		// TODO Testausgabe
		System.out.println("datum aus mail   " + emailDatum);
		System.out.println("datum aus confdb " + confDatum);
		if (confDatum >= emailDatum) {
			return true;
		}
		return false;

	}

}
package com.sabel.bilderrahmen.Mail;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.Date;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.ContentType;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.ParseException;

import com.sabel.bilderrahmen.db.Bild;
import com.sabel.bilderrahmen.db.BilderService;
import com.sabel.bilderrahmen.db.Config;
import com.sabel.bilderrahmen.db.ConfigService;

/**
 * 
 * @author Alexander Brunzel
 *
 */

// TODO behaupte funktioniert nicht getestet
public class EmailAuswerten {

	public EmailAuswerten() {

	}

	// Mailtext zerstueckeln und pruefen ob es eine config ist
	public void inhaltZerteilen(Message mss) throws Exception {
		// text[0] soll erste zeile der mail sein ueberschuessige \r entfernen
		String text = mimeTextasulesen(mss);
		text = text.replaceAll("\r", "");
		text = text.replaceAll(" ", "");
		String[] parts = text.split("\n");
		Long emailDatum = mss.getSentDate().getTime();
		// wenns ne config ist
		if (parts.length >= 1 && (parts[0].equals("config"))) {
			configFormatieren(parts, mss.getSentDate());

		} else {
			// TODO Testausgabe
			System.out.println("keine config, ueberpruefe ob Anlagen vorhanden");
			Multipart mp = (Multipart) mss.getContent();
			anlagenAuslesen(mp, emailDatum);
		}
	}

	private void anlagenAuslesen(Multipart mp, Long emailDatum)
			throws MessagingException, IOException, FileNotFoundException {
		// solang anlagen vorhanden
		int bildID = 0;
		for (int e = 0; e < mp.getCount(); e++) {
			BodyPart bodyPart = mp.getBodyPart(e);
			if (Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
				InputStream is = bodyPart.getInputStream();
				// letzen 3 zeichen jpg? sind egal wie geschrieben
				if (bodyPart.getFileName().substring(bodyPart.getFileName().length() - 3).equalsIgnoreCase("jpg")) {
					// Daten im Verzeichniss Bilder speichern
					Bild b = new Bild();
					BilderService bs = new BilderService();
					bildID = bs.gibLetztesBild().getBildID() + 1;

					System.out.println(bildID);
					File f = new File("Bilder/Bild" + bildID + ".jpg");
					System.out.println(f + f.getName());
					String bildname = f.getName();
					// bild in db speichern
					b.setPfadOriginal(f.getPath());
					b.setBildID(bildID);
					b.setDatum(emailDatum.toString());

					// Bild schreiben auf hdd
					FileOutputStream fos = new FileOutputStream(f);
					byte[] buf = new byte[4096];
					int bytesRead;
					while ((bytesRead = is.read(buf)) != -1) {
						fos.write(buf, 0, bytesRead);
					}
					fos.close();
					// bilder nach erfolgreichem einlesen skalieren
					b.skalieren(bildname);
					b.setPfadSkaliert("Skalierte_" + f.getPath());
					bs.speichern(b);
					// TODO testausgabe
					System.out.println("Bild " + e + " erfolgreich eingelesen und skalliert");
					// TODO testelse
				} else {

					System.out.println("kein jpg " + bodyPart.getFileName()
							.substring(bodyPart.getFileName().length() - 3).equalsIgnoreCase("jpg"));
				}
			}
		}
	}

	// auslesen welchen Mimetyp die email hat und diese wird weitergegeben
	private String mimeTypeAuslesen(BodyPart part) throws ParseException, MessagingException, IOException {
		String text = "";
		ContentType ct = new ContentType(part.getContentType());
		String type = ct.getPrimaryType() + "/" + ct.getSubType();
		type = type.toUpperCase();
		if (type.equals("TEXT/PLAIN")) {
			text += part.getContent().toString();
		} else if (type.startsWith("MULTIPART")) {
			Multipart mPart = (Multipart) part.getContent();

			for (int e = 0; e < mPart.getCount(); e++) {
				BodyPart bodyPart = mPart.getBodyPart(e);
				text += mimeTypeAuslesen(bodyPart);
			}
		}
		// Brauch ich vielleicht noch
		// else if (type.equals("TEXT/HTML")) {
		// text += HTML2Text.convert(part.getContent().toString());
		// text += "htmlMessage\n";
		// }
		return text;
	}

	// text der email aus mime lesen
	private String mimeTextasulesen(Message m) throws Exception {
		String text = "";

		MimeMultipart content = (MimeMultipart) m.getContent();

		for (int i = 0; i < content.getCount(); i++) {
			BodyPart part = content.getBodyPart(i);
			text += mimeTypeAuslesen(part);
		}
		return text;
	}

	// Formatieren nach Muster: anzahlbilder, anzeigedauer, ladezeit uebergabe
	// an Config
	private static Boolean configFormatieren(String[] parts, Date date) throws Exception {
		// Aufteilen des Inhalts am komma
		Config conf = new Config();
		ConfigService cs = new ConfigService();
		conf = cs.gibLetzteConfig();

		String[] cfgData = parts[1].split(",");
		int anzahlBilder = 0;
		int anzeigedauer = 0;
		int ladezeit = 0;
		try {
		if (cfgData[0].equals("*")) {
			anzahlBilder = Integer.MAX_VALUE;
		}else{
			anzahlBilder = Integer.parseInt(cfgData[0]);
		}
			anzeigedauer = Integer.parseInt(cfgData[1]);
			ladezeit = conf.getLadezeit();
		} catch (Exception e) {
			anzahlBilder = -1;
			anzeigedauer = -1;
			ladezeit = -1;
			System.out.println("Bitte nur Zahlen angeben im Format anzahlbilder,anzeigedauer,ladezeit");
			System.out.println("mindestens 3 Bilder, hoechstens alle 3 sekunden, ladezeit min 5 minuten");
		}

		if (cfgData.length == 3) {
			try {
				ladezeit = Integer.parseInt(cfgData[2]);
			} catch (Exception e) {
				anzahlBilder = -1;
				anzeigedauer = -1;
				ladezeit = -1;
				System.out.println("Bitte nur Zahlen angeben im Format anzahlbilder,anzeigedauer,ladezeit");
				System.out.println("mindestens 3 Bilder, hoechstens alle 3 sekunden, ladezeit min 5 minuten");
			}

			if ((ladezeit > 4 && ladezeit < 180) && (anzeigedauer > 2 && anzeigedauer < 61) && (anzahlBilder > 2)) {

				conf.setLadezeit(ladezeit * 1000 * 60); // von Min in ms
				conf.setAnzeigedauer(anzeigedauer * 1000); // von sek in ms
				conf.setAnzahlBilder(anzahlBilder);
				conf.setDatum(Long.toString(date.getTime()));

				cs.speichern(conf);

				// TODO testausgabe
				System.out.println(conf);
				System.out.println("--------------");
				System.out.println("ist 3 parameter lang");
				System.out.println("Abgesendet:   " + date.getTime());
				System.out.println("anzeigedauer: " + anzeigedauer);
				System.out.println("anzahlbilder  " + anzahlBilder);
				System.out.println("ladezeit " + ladezeit);
				return true;

			}
		}
		if (cfgData.length == 2) {
			if ((anzeigedauer > 2 && anzeigedauer < 61) && (anzahlBilder > 2)) {

				conf.setLadezeit(ladezeit); // Ladezeit bleibt gleich
				conf.setAnzeigedauer(anzeigedauer * 1000); // von sek in ms
				conf.setAnzahlBilder(anzahlBilder);
				conf.setDatum(Long.toString(date.getTime()));
				cs.speichern(conf);

				// TODO testausgabe
				System.out.println(conf);
				System.out.println("--------------");
				System.out.println("ist 2 parameter lang");
				System.out.println("Abgesendet:   " + date.getTime());
				System.out.println("anzeigedauer: " + anzeigedauer);
				System.out.println("anzahlbilder  " + anzahlBilder);
				System.out.println("ladezeit " + ladezeit);

				return true;
			} else {
				// TODO Testelse
				System.out.println("Falsche Parameter " + anzeigedauer + " anzahlbilder " + anzahlBilder + " ladezeit "
						+ ladezeit);
				return false;
			}

		} else {
			// TODO Testelse
			System.out.println(" ist nicht 2 parameter lang " + anzeigedauer + " anzahlbilder " + anzahlBilder
					+ " ladezeit " + ladezeit);
			System.out.println("Anzahl der Parameter (muss 2 oder 3 sein): " + cfgData.length);
			return false;
		}
	}
}
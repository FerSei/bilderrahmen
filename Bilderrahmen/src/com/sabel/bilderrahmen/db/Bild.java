package com.sabel.bilderrahmen.db;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.swing.Icon;

/**
 * 
 * @author Ferdinand Seibold, Dominic Janda
 *
 */

@Entity
public class Bild implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private int bildID;
	private String pfadOriginal;
	private String pfadSkaliert;
	private String bildDatum;
	@Transient
	private Icon icon;

	public Bild() {

	}

	public Bild(String pfad) {
		super();
		this.pfadOriginal = pfad;
	}

	public Bild(String originalpfad, String skaliertpfad) {
		super();
		this.pfadOriginal = originalpfad;
		this.pfadSkaliert = skaliertpfad;
		this.bildDatum = aktuellesDatum();
	}

	public String getPfadOriginal() {
		return pfadOriginal;
	}

	public void setPfadOriginal(String pfadOriginal) {
		this.pfadOriginal = pfadOriginal;
	}

	public String getPfadSkaliert() {
		return pfadSkaliert;
	}

	public void setPfadSkaliert(String pfadSkaliert) {
		this.pfadSkaliert = pfadSkaliert;
	}

	public void setBildID(int bildID) {
		this.bildID = bildID;
	}

	public int getBildID() {
		return bildID;
	}

	public Icon getIcon() {
		return icon;
	}

	public void setIcon(Icon icon) {
		this.icon = icon;
	}

	public String getDatum() {
		return bildDatum;
	}

	public void setDatum(String datum) {
		this.bildDatum = datum;
	}

	// Gibt das aktuelle Datum in Millisekunden zurueck
	public String aktuellesDatum() {
		Date date = Calendar.getInstance().getTime();
		return Long.toString(date.getTime());
	}

	// Bild wird auf die Auflösung des Monitors skaliert
	public void skalieren(String bildname) throws IOException {
		BufferedImage bild = ImageIO.read(new File(getPfadOriginal()));

		// Gib die Auflösung des Monitors
		Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
		int width = (int) size.getWidth();
		int height = (int) size.getHeight();
		int bildWidth = bild.getWidth();
		int bildHeight = bild.getHeight();
		double skalWidth = 0;
		double skalHeight = 0;

		// Setze neue Höhe und Breite des skalierten Bildes
		if (bildWidth == bildHeight) {
			skalWidth = width;
			skalHeight = height;
		} else if (bildWidth > bildHeight) {
			skalWidth = width;
			skalHeight = ((double) bildHeight / (double) bildWidth) * width;
		} else {
			skalHeight = height;
			skalWidth = ((double) bildWidth / (double) bildHeight) * height;
		}

		Image skaliertesBild = bild.getScaledInstance((int) skalWidth, (int) skalHeight, Image.SCALE_DEFAULT);

		// Erstelle neues skaliertes Bild
		BufferedImage neuesBild = new BufferedImage((int) skalWidth, (int) skalHeight, BufferedImage.TYPE_INT_RGB);
		neuesBild.getGraphics().drawImage(skaliertesBild, 0, 0, null);
		Graphics g = neuesBild.getGraphics();
		g.drawImage(skaliertesBild, 0, 0, null);
		g.dispose();

		setPfadSkaliert("Skalierte_Bilder/" + bildname);
		ImageIO.write(neuesBild, "jpg", new File(getPfadSkaliert()));

	}

	@Override
	public String toString() {
		return "Bild [bildID=" + bildID + ", pfadOriginal=" + pfadOriginal + ", pfadSkaliert=" + pfadSkaliert
				+ ", icon=" + icon + "]";
	}
}

package com.sabel.bilderrahmen.db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Ferdinand Seibold, Dominic Janda
 *
 */

public class AdminPasswortService {
	private EntityManagerFactory emf;
	private EntityManager em;

	public AdminPasswortService() {
		emf = Persistence.createEntityManagerFactory("Bilderrahmen");
		em = emf.createEntityManager();
	}

	public void close() {
		if (em != null) {
			em.close();
		}
		em = null;
		if (emf != null) {
			emf.close();
		}
		emf = null;
	}

	public void speichern(AdminPasswort adminPasswort) {
		em.getTransaction().begin();
		em.persist(adminPasswort);
		em.getTransaction().commit();
	}

	public AdminPasswort gibConfig(int adminPasswortID) {
		return em.find(AdminPasswort.class, adminPasswortID);
	}

	/**
	 * Der letzte Eintrag aus der DB (Tabelle Adminpasswort) wird zurückgegeben
	 * 
	 * @return AdminPasswort
	 */
	public AdminPasswort gibLetztesAdminPasswort() {
		TypedQuery<AdminPasswort> tq = em.createQuery(
				"SELECT co FROM AdminPasswort co WHERE co.adminPasswortID=(SELECT Max(c.adminPasswortID) FROM AdminPasswort c)",
				AdminPasswort.class);
		return tq.getSingleResult();
	}
}

package com.sabel.bilderrahmen.db;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 
 * @author Ferdinand Seibold, Dominic Janda
 *
 */

@Entity
public class Config {
	@Id
	@GeneratedValue
	private int configID;
	private String geraetename;
	private int anzeigedauer; // in Millisekunden
	private int anzahlBilder;
	private String configDatum;
	private int ladezeit; // in Millisekunden

	public Config() {

	}

	public String aktuellesDatum() {
		Date date = Calendar.getInstance().getTime();
		return Long.toString(date.getTime());
	}

	public String getGeraetename() {
		return geraetename;
	}

	public int getAnzeigedauer() {
		return anzeigedauer;
	}

	public void setAnzeigedauer(int anzeigedauer) {
		this.anzeigedauer = anzeigedauer;
	}

	public void setGeraetename(String geraetename) {
		this.geraetename = geraetename;
	}

	public int getAnzahlBilder() {
		return anzahlBilder;
	}

	public void setAnzahlBilder(int anzahlBilder) {
		this.anzahlBilder = anzahlBilder;
	}

	public String getDatum() {
		return configDatum;
	}

	public void setDatum(String datum) {
		this.configDatum = datum;
	}

	public int getLadezeit() {
		return ladezeit;
	}

	public void setLadezeit(int ladezeit) {
		this.ladezeit = ladezeit;
	}

	@Override
	public String toString() {
		return "Config [configID=" + configID + ", geraetename=" + geraetename + ", anzeigedauer=" + anzeigedauer
				+ ", anzahlBilder=" + anzahlBilder + ", datum=" + configDatum + ", ladezeit=" + ladezeit + "]";
	}

}

package com.sabel.bilderrahmen.db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Ferdinand Seibold, Dominic Janda
 *
 */

public class EmailService {
	private EntityManagerFactory emf;
	private EntityManager em;

	public EmailService() {
		emf = Persistence.createEntityManagerFactory("Bilderrahmen");
		em = emf.createEntityManager();
	}

	public void close() {
		if (em != null) {
			em.close();
		}
		em = null;
		if (emf != null) {
			emf.close();
		}
		emf = null;
	}

	public void speichern(Email email) {
		em.getTransaction().begin();
		em.persist(email);
		em.getTransaction().commit();
	}

	public Email gibEmail(int emailID) {
		return em.find(Email.class, emailID);
	}

	/**
	 * Methode, die die letzte Email aus der DB (Tabelle Email) ausgibt
	 * 
	 * @return Email
	 */
	public Email gibLetzteEmail() {
		TypedQuery<Email> tq = em.createQuery(
				"SELECT co FROM Email co WHERE co.emailID=(SELECT Max(c.emailID) FROM Email c)", Email.class);
		return tq.getSingleResult();
	}

}

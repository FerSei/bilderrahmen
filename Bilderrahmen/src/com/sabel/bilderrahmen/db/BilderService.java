package com.sabel.bilderrahmen.db;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Ferdinand Seibold, Dominic Janda
 *
 */

public class BilderService {
	private EntityManagerFactory emf;
	private EntityManager em;

	public BilderService() {
		emf = Persistence.createEntityManagerFactory("Bilderrahmen");
		em = emf.createEntityManager();
	}

	public void close() {
		if (em != null) {
			em.close();
		}
		em = null;
		if (emf != null) {
			emf.close();
		}
		emf = null;
	}

	public void speichern(Bild bild) {
		em.getTransaction().begin();
		em.persist(bild);
		em.getTransaction().commit();
	}

	public Bild gibBild(int bildID) {
		return em.find(Bild.class, bildID);
	}

	/**
	 * Methode, welche alle Bilder aus der DB (Tabelle Bild) zurückgibt
	 * @return List - Liste mit Bildern
	 */
	public List<Bild> gibAlleBilder() {
		TypedQuery<Bild> tq = em.createQuery("SELECT bi FROM Bild bi", Bild.class);
		return tq.getResultList();
	}

	/**
	 * Methode, welche das letzte Bild aus der DB (Tabelle Bild) zurückgibt
	 * @return Bild
	 */
	public Bild gibLetztesBild() {
		TypedQuery<Bild> tq = em
				.createQuery("SELECT bi FROM Bild bi WHERE bi.bildID=(SELECT Max(b.bildID) FROM Bild b)", Bild.class);
		return tq.getSingleResult();
	}

	/**
	 * Methode, welche das erste Bild aus der DB (Tabelle Bild) zurückgibt
	 * @return Bild
	 */
	public Bild gibErstesBild() {
		TypedQuery<Bild> tq = em
				.createQuery("SELECT bi FROM Bild bi WHERE bi.bildID=(SELECT Min(b.bildID) FROM Bild b)", Bild.class);
		return tq.getSingleResult();
	}

	/**
	 * Loesche Bild aus dem Ordner "Skalierte Bilder" und aus der DB
	 * @param bild
	 */
	public void loescheBild(Bild bild) {
		File file = new File(bild.getPfadSkaliert());
		if (file.exists()) {
			file.delete();
			em.getTransaction().begin();
			em.remove(bild);
			em.getTransaction().commit();
			return;
		}
	}

	/**
	 * Ueberpruefe ob im Ordner "Skalierte Bilder" Bilder vorhanden sind
	 * 
	 * @return boolean true = skalierte Bilder sind vorhanden, false = keine skalierten Bilder vorhanden
	 */
	public boolean uerpruefeSkalierteBilder() {
		File f = new File("Skalierte_Bilder");
		File[] fileArray = f.listFiles();
		if (fileArray.length == 0) {
			/**
			 * Es sind keine skalierten Bilder vorhanden
			 */
			return false;
		}
		/**
		 * Es sind skalierte Bilder vorhanden
		 */
		return true;
	}

	/**
	 * Erstellt für alle Bilder im Ordner "Bilder" skalierte Bilder in den
	 * Ordner "Skalierte Bilder"
	 * 
	 * @throws IOException
	 */
	public void erstelleSkalierteBilder() throws IOException {
		if (!uerpruefeSkalierteBilder()) {
			/**
			 * Wenn keine skalierten Bilder vorhanden sind
			 */
			File file = new File("Bilder");
			File[] fileArray = file.listFiles();
			for (int i = 0; i < fileArray.length; i++) {
				Bild bild = new Bild();
				BilderService bilderservice = new BilderService();
				int bildID = i + 1;
				File f = new File("Bilder/Bild" + bildID + ".jpg");
				String bildname = f.getName();
				bild.setPfadOriginal(f.getPath());
				bild.setDatum(bild.aktuellesDatum());
				bild.skalieren(bildname);
				bilderservice.speichern(bild);
			}
		}
	}

}

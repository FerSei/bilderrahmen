package com.sabel.bilderrahmen.db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * 
 * @author Ferdinand Seibold, Dominic Janda
 *
 */

public class ConfigService {
	private EntityManagerFactory emf;
	private EntityManager em;

	public ConfigService() {
		emf = Persistence.createEntityManagerFactory("Bilderrahmen");
		em = emf.createEntityManager();
	}

	public void close() {
		if (em != null) {
			em.close();
		}
		em = null;
		if (emf != null) {
			emf.close();
		}
		emf = null;
	}

	public void speichern(Config config) {
		em.getTransaction().begin();
		em.persist(config);
		em.getTransaction().commit();
		System.out.println("Config wurde gespeichert");
	}

	public Config gibConfig(int configID) {
		return em.find(Config.class, configID);
	}

	/**
	 * Methode, welche die letzte Config aus der DB (Tabelle Config) zurückgibt
	 * @return Config
	 */
	public Config gibLetzteConfig() {
		TypedQuery<Config> tq = em.createQuery(
				"SELECT co FROM Config co WHERE co.configID=(SELECT Max(c.configID) FROM Config c)", Config.class);
		return tq.getSingleResult();
	}
}

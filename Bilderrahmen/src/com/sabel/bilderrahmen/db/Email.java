package com.sabel.bilderrahmen.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 
 * @author Ferdinand Seibold, Dominic Janda
 *
 */

@Entity
public class Email {
	@Id
	@GeneratedValue
	private int emailID;
	private String emailAdresse;
	private String passwort;

	public Email() {
		// TODO Auto-generated constructor stub
	}

	public String getEmailAdresse() {
		return emailAdresse;
	}

	public void setEmailAdresse(String emailAdresse) {
		this.emailAdresse = emailAdresse;
	}

	public String getPasswort() {
		return passwort;
	}

	public void setPasswort(String passwort) {
		this.passwort = passwort;
	}

	@Override
	public String toString() {
		return "Email [emailID=" + emailID + ", emailAdresse=" + emailAdresse + ", passwort=" + passwort + "]";
	}

}

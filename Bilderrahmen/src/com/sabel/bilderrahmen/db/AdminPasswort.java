package com.sabel.bilderrahmen.db;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * 
 * @author Ferdinand Seibold, Dominic Janda
 *
 */

@Entity
public class AdminPasswort {
	@Id
	@GeneratedValue
	private int adminPasswortID;
	private String adminPasswort;

	public AdminPasswort() {
		// TODO Auto-generated constructor stub
	}

	public String getAdminPasswort() {
		return adminPasswort;
	}

	public void setAdminPasswort(String adminPasswort) {
		this.adminPasswort = adminPasswort;
	}
}

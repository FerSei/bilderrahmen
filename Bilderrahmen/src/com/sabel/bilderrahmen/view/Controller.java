package com.sabel.bilderrahmen.view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.sabel.bilderrahmen.db.Bild;
import com.sabel.bilderrahmen.db.BilderService;
import com.sabel.bilderrahmen.db.Config;
import com.sabel.bilderrahmen.db.ConfigService;

/**
 * 
 * @author Ferdinand Seibold
 *
 */

public class Controller {
	
	private Config config;
	private ConfigService configService;
	private BilderService bilderService;
	private int aktuelleID;
	private int kleinsteID;
	private int hoechsteID;

	public Controller() {
		super();
		configService = new ConfigService();
		config = configService.gibLetzteConfig();
		bilderService = new BilderService();
		kleinsteID = bilderService.gibErstesBild().getBildID();
		hoechsteID = bilderService.gibLetztesBild().getBildID();
		resetZaehler();
	}
	
	//Gibt das naechste Bild zurueck
	public ImageIcon neuesBildGeben() {
		reloadConfig();
		//Wenn weniger Bilder im Ordner "Skalierte Bilder" als AnzahlBilder in der Config sind
		if(pruefeZaehler().equals("WenigerBilderAlsInConfig")) {
			if(aktuelleID < kleinsteID) {
				resetZaehler();
			}
			Bild naechstesBild = bilderService.gibBild(this.aktuelleID);
			ImageIcon naechstesIcon = new ImageIcon(naechstesBild.getPfadSkaliert());
			this.aktuelleID--;
			return naechstesIcon;
		}
		//Wenn mehr Bilder im Ordner "Skalierte Bilder" als AnzahlBilder in der Config sind
		if (pruefeZaehler().equals("MehrBilderAlsInConfig")) {
			if (aktuelleID <= hoechsteID - config.getAnzahlBilder()) {
				resetZaehler();
			}
			Bild naechstesBild = bilderService.gibBild(this.aktuelleID);
			ImageIcon naechstesIcon = new ImageIcon(naechstesBild.getPfadSkaliert());
			this.aktuelleID--;
			return naechstesIcon;
		}
		//Fuer den Fall der Faelle, dass beide vorherigen nicht zutreffen: Erstes Bild wieder ausgeben / Von vorne anfangen
		resetZaehler();
		Bild naechstesBild = bilderService.gibBild(this.aktuelleID);
		ImageIcon naechstesIcon = new ImageIcon(naechstesBild.getPfadSkaliert());
		this.aktuelleID--;
		return naechstesIcon;
	}
	
	//Holt sich die neueste Config aus der DB
	public void reloadConfig() {
		config = configService.gibLetzteConfig();
		pruefeZaehler();
	}

	public Config getConfig() {
		return config;
	}
	
	public BilderService getBilderService() {
		return bilderService;
	}
	
	//Setzt den Zaehler für die BildID auf das Erste Bild zurueck
	private void resetZaehler() {
		this.aktuelleID = bilderService.gibLetztesBild().getBildID();
	}
	
	//Prueft, ob der Zaehler der BildID groesser oder kleiner ist als die AnzahlBilder in der neuesten Config
	private String pruefeZaehler() {
		int anzahlBilder = bilderService.gibAlleBilder().size();
		int anzahlInConfig = configService.gibLetzteConfig().getAnzahlBilder();
		
		if (anzahlBilder < anzahlInConfig) {
			return "WenigerBilderAlsInConfig";
		}
		return "MehrBilderAlsInConfig";
	}
	
	public boolean loescheSkalierteBilder() throws IOException {
		//Alle Bilder werden vor Bilderlauf gelöscht, wenn im Skalierte_Bilder-Ordner Bilder vorhanden sind
		if(getBilderService().uerpruefeSkalierteBilder()){
			List<Bild> alleBilder = getBilderService().gibAlleBilder();
			for (Bild bild : alleBilder) {
				BufferedImage b = ImageIO.read(new File(bild.getPfadOriginal()));
				int bildBreite = b.getWidth();
				int bildHoehe = b.getHeight();
				Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
				int monitorBreite = (int) size.getWidth();
				int monitorHoehe = (int) size.getHeight();
				if(bildBreite == monitorBreite && bildHoehe == monitorHoehe) {
					getBilderService().loescheBild(bild);
					System.out.println("Bilder wurden gelöscht");
					return true;
				}
				System.out.println("Bilder sind schon in der richtigen Größe");
				return false;
			}
		}
		return false;
	}
	
	public void skaliereAlleBilder(boolean wurdenBilderGeloescht) {
		if(wurdenBilderGeloescht) {
			//Alle Bilder werden vor Bilderlauf neu skaliert
			try {
				getBilderService().erstelleSkalierteBilder();
			} catch (IOException e1) {
//				Startfenster startfenster = new Startfenster();
//				JOptionPane.showMessageDialog(startfenster, "Es sind keine Bilder zum skalieren vorhanden. Bitte fügen Sie Bilder in den Bilder-Ordner hinzu.",
//					"Es ist ein Fehler aufgetreten", JOptionPane.WARNING_MESSAGE);
			}
			System.out.println("Bilder wurden skaliert");
		}
		
	}
	
	

}

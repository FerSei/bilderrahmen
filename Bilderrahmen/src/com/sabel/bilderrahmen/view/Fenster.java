package com.sabel.bilderrahmen.view;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import com.sabel.bilderrahmen.view.Windows.Startfenster;

/**
 * 
 * @author Ferdinand Seibold, Dominic Janda
 *
 */
@SuppressWarnings("serial")
public class Fenster extends JFrame {

	private Container c;
	private JPanel jpCenter;
	private JLabel jlabel;
	private Controller controller;
	private Startfenster startfenster;
	private Timer timer;

	public Fenster(Controller controller) {
		super();
		this.controller = controller;
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.initComponents();
		this.initEvents();
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		if (gd.isFullScreenSupported()) {
			setUndecorated(true);
			gd.setFullScreenWindow(this);
		} else {
			System.err.println("Fullscreen not supported");
			setSize(1024, 768);
			this.setVisible(true);
		}
		this.setVisible(true);
	}

	private void initComponents() {
		/**
		 * Startfenster wird hier erstellt und im Anschluss gleich unsichtbar
		 * gemacht (mit Q wird es sichtbar)
		 */
		startfenster = new Startfenster();
		startfenster.setVisible(false);

		/**
		 * Timer für Dauer der Bildanzeige wird erstellt
		 */
		timer = new Timer();

		/**
		 * Das Fenster mit der Bildanzeige wird erstellt
		 */
		c = getContentPane();
		jpCenter = new JPanel(new GridLayout(1, 1));
		jlabel = new JLabel(controller.neuesBildGeben());
		jpCenter.add(jlabel);
		jpCenter.setBackground(Color.BLACK);
		c.add(jpCenter);
		mauszeigerVerstecken();
		mauszeigerMittigSetzen();
	}

	private void initEvents() {
		SwingUtilities.invokeLater(new Runnable() {
			/**
			 * holt sich die anzeigedauer aus der aktuellen Config
			 */
			int anzeigedauer = controller.getConfig().getAnzeigedauer();

			@Override
			public void run() {
				/**
				 * Timer zum Anzeigen der Bilder, Intervall ist die Anzeigedauer
				 */
				timer.schedule(new TimerTask() {

					@Override
					public void run() {
						/**
						 * Wenn die Anzeigedauer der Bilder in der neuesten
						 * Config nicht die aktuelle Anzeigedauer ist wird der
						 * Timer gestoppt und das Fenster neu gestartet
						 */
						int anzeigedauerNeu = controller.getConfig().getAnzeigedauer();
						if (anzeigedauer != anzeigedauerNeu) {
							cancel();
							dispose();
							Controller controller = new Controller();
							new Fenster(controller);
						}
						/**
						 * setzt das naechste Bild auf das jlabel
						 */
						ImageIcon naechstesBild = controller.neuesBildGeben();
						jlabel.setIcon(naechstesBild);
					}
				}, 0, anzeigedauer);
			}
		});
		/**
		 * Bei Tastendruck "Q" wird das Startfenster mit der aktuellen Config
		 * sichtbar gemacht
		 */
		jpCenter.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_Q, 0),
				"forward");
		jpCenter.getActionMap().put("forward", new AbstractAction() {

			@Override
			public void actionPerformed(ActionEvent e) {
				startfenster.setVisible(true);
				mauszeigerMittigSetzen();
			}
		});
	}

	/**
	 * Methode um den Mauszeiger auf dem Panel nicht anzuzeigen
	 */
	public void mauszeigerVerstecken() {
		this.setCursor(java.awt.Toolkit.getDefaultToolkit().createCustomCursor(
				new BufferedImage(1, 1, BufferedImage.TYPE_4BYTE_ABGR), new java.awt.Point(0, 0), "NOCURSOR"));
	}

	/**
	 * Methode um den Mauszeiger auf dem Panel anzuzeigen
	 */
	public void mauszeigerAnzeigen() {
		this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	/**
	 * Methode um den Mauszeiger in der Mitte des Bildschirms zu setzen
	 */
	public void mauszeigerMittigSetzen() {
		try {
			Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
			int width = (int) size.getWidth() / 2;
			int height = (int) size.getHeight() / 2;
			Robot robot = new Robot();
			robot.mouseMove(width, height);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
}

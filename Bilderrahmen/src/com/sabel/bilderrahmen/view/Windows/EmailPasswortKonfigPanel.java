package com.sabel.bilderrahmen.view.Windows;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.sabel.bilderrahmen.db.Email;

/**
 * 
 * @author Dominic Janda
 *
 */

public class EmailPasswortKonfigPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JPanel jpEmail, jpNeuesPw, jpNeuesPwWiederholung, jpButton;
	private JLabel jlNeuesePwWiederholung, jlNeuesPw, jlEmail;
	private JPasswordField jpassNeu, jpassNeuWiederholung;
	private JTextField jtxtEmail;

	public EmailPasswortKonfigPanel() {
		this.initComponents();
		this.deaktiviereTextfelder();
	}

	private void initComponents() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		jpEmail = new JPanel();
		jpNeuesPw = new JPanel();
		jpNeuesPwWiederholung = new JPanel();
		jpButton = new JPanel();

		jlEmail = new JLabel("E-Mail:");
		jlNeuesPw = new JLabel("neues E-Mail Passwort:");
		jlNeuesePwWiederholung = new JLabel("Wiederholen:");

		jtxtEmail = new JTextField(20);
		jpassNeu = new JPasswordField(20);
		jpassNeuWiederholung = new JPasswordField(20);

		jpEmail.add(jlEmail);
		jpEmail.add(jtxtEmail);
		jpNeuesPw.add(jlNeuesPw);
		jpNeuesPw.add(jpassNeu);
		jpNeuesPwWiederholung.add(jlNeuesePwWiederholung);
		jpNeuesPwWiederholung.add(jpassNeuWiederholung);

		jpEmail.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jpNeuesPw.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jpNeuesPwWiederholung.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jpButton.setLayout(new FlowLayout(FlowLayout.CENTER));

		this.add(jpEmail);
		this.add(jpNeuesPw);
		this.add(jpNeuesPwWiederholung);
		this.add(jpButton);
	}

	/**
	 * Methode, um die letzte Email-Adresse aus dem DB auszulesen
	 * 
	 * @param email
	 */
	public void setzeEmail(Email email) {
		jtxtEmail.setText(email.getEmailAdresse());
	}

	/**
	 * Methode, um die Eingabe in die DB zu schreiben
	 * 
	 * @return Email - Email Datensatz
	 */
	public Email schreibeInEmailDB() {
		Email email = new Email();
		char[] passwortEingabeZeichen = this.jpassNeu.getPassword();
		String passwortEingabe = new String(passwortEingabeZeichen);
		email.setPasswort(passwortEingabe);
		email.setEmailAdresse(jtxtEmail.getText());
		return email;
	}

	/**
	 * Methode, um die Textfelder mit Klick auf JToggleButton zu aktivieren
	 */
	public void aktiviereTextfelder() {
		jtxtEmail.setEnabled(true);
		jpassNeu.setEnabled(true);
		jpassNeuWiederholung.setEnabled(true);
	}

	/**
	 * Methode, um die Textfelder mit Klick auf JToggleButton zu dekativieren
	 */
	public void deaktiviereTextfelder() {
		jtxtEmail.setEnabled(false);
		jpassNeu.setEnabled(false);
		jpassNeuWiederholung.setEnabled(false);
	}

	/**
	 * Methode um die Eingaben zu prüfen
	 * 
	 * @return true = Eingaben sind korrekt, false = Eingaben sind nicht korrekt
	 */
	public boolean eingabenUeberpruefen() {
		String email = this.jtxtEmail.getText().trim();
		String passwort = new String(this.jpassNeu.getPassword()).trim();
		String passwortWiederholung = new String(this.jpassNeuWiederholung.getPassword()).trim();
		if (!email.isEmpty() && passwort.equals(passwortWiederholung) && !passwort.isEmpty() && email.contains("@")
				&& email.contains(".")) {
			/**
			 * Es wird geprüft ob: 
			 * - Das Feld Email nicht leer ist 
			 * - Die beiden eingegebenen Passwörter gleich sind 
			 * - Das Passwort nicht leer ist
			 * - Das Feld Email sowohl ein @ als auch ein . enthält
			 */
			JOptionPane.showMessageDialog(EmailPasswortKonfigPanel.this, "Die Aenderungen wurden gespeichert", "Erfolg",
					JOptionPane.INFORMATION_MESSAGE);
			return true;
		}
		return false;
	}
}

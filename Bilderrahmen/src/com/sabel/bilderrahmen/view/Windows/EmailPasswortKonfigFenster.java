package com.sabel.bilderrahmen.view.Windows;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.sabel.bilderrahmen.db.Email;
import com.sabel.bilderrahmen.db.EmailService;
import com.sabel.bilderrahmen.view.Buttons.StandardJButtons;
import com.sabel.bilderrahmen.view.Buttons.StandardJToggleButtons;

/**
 * 
 * @author Dominic Janda
 *
 */

public class EmailPasswortKonfigFenster extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Container c;
	private StandardJButtons jbtnOk;
	private StandardJToggleButtons jtbtnBearbeiten;
	private JLabel jlHeader;
	private JPanel jpSouth, jpHeader;
	private EmailService emailPasswortService;
	private EmailPasswortKonfigPanel emailPasswortPanel;
	private ImageIcon titleImage;

	public EmailPasswortKonfigFenster() {
		titleImage = new ImageIcon("PanelImages/TitleImage.png");
		this.setIconImage(titleImage.getImage());
		this.setTitle("PiLderrahmen V1.0");
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.initComponents();
		this.initEvents();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void initEvents() {
		/**
		 * Eventhandling für den Button "OK"
		 */
		jbtnOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		/**
		 * Eventhandling für den Button Bearbeiten
		 */
		jtbtnBearbeiten.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (jtbtnBearbeiten.isSelected()) {
					/**
					 * Wenn der ToggleButton Bearbeiten gedrückt wurde
					 */
					jtbtnBearbeiten.setIcon(new ImageIcon("PanelImages/Done.png"));
					emailPasswortPanel.aktiviereTextfelder();
					jbtnOk.setIcon(new ImageIcon("PanelImages/Back.png"));
				} else {
					/**
					 * Wenn der ToggleButton Bearbeiten wieder losgelassen wird
					 */
					Email email = emailPasswortPanel.schreibeInEmailDB();
					if (emailPasswortPanel.eingabenUeberpruefen()) {
						/**
						 * Wenn die Eingaben stimmen
						 */
						emailPasswortPanel.schreibeInEmailDB();
						emailPasswortService.speichern(email);
						jtbtnBearbeiten.setIcon(new ImageIcon("PanelImages/ConfigChange.png"));
						jbtnOk.setIcon(new ImageIcon("PanelImages/Done.png"));
						emailPasswortPanel.deaktiviereTextfelder();
						dispose();
					} else {
						/**
						 * Wenn die Eingaben nicht stimmen
						 */
						JOptionPane.showMessageDialog(EmailPasswortKonfigFenster.this,
								"Bitte überprüfen Sie Ihre Eingaben", "Es ist ein Fehler aufgetreten",
								JOptionPane.WARNING_MESSAGE);
						jtbtnBearbeiten.setSelected(true);
					}
				}
			}
		});
	}

	private void initComponents() {
		c = this.getContentPane();
		emailPasswortService = new EmailService();
		emailPasswortPanel = new EmailPasswortKonfigPanel();

		/**
		 * Header wird gesetzt
		 */
		File headerFile = new File("PanelImages/HeaderEmail.png");
		BufferedImage image = null;
		try {
			image = ImageIO.read(headerFile);
		} catch (IOException e) {
			System.out.println("Headerimage nicht gefunden");
		}

		jpSouth = new JPanel();
		jpHeader = new JPanel();
		jlHeader = new JLabel(new ImageIcon(image));
		jpHeader.add(jlHeader);

		c.add(jpHeader, BorderLayout.NORTH);
		c.add(emailPasswortPanel);
		c.add(jpSouth, BorderLayout.SOUTH);

		/**
		 * Der letzte Datensatz aus der DB (Tabelle Email) wird angezeigt
		 */
		emailPasswortPanel.setzeEmail(emailPasswortService.gibLetzteEmail());

		/**
		 * Die ImageIcons werden auf die Buttons gesetzt
		 */
		try {
			BufferedImage buttonIconBearbeiten;
			buttonIconBearbeiten = ImageIO.read(new File("PanelImages/ConfigChange.png"));
			jtbtnBearbeiten = new StandardJToggleButtons(new ImageIcon(buttonIconBearbeiten));

			BufferedImage buttonIconFertig;
			buttonIconFertig = ImageIO.read(new File("PanelImages/Done.png"));
			jbtnOk = new StandardJButtons(new ImageIcon(buttonIconFertig));
		} catch (IOException e) {
			/**
			 * Falls die ImageIcons nicht vorhanden sind, wird der Text gesetzt
			 */
			jtbtnBearbeiten = new StandardJToggleButtons("Ändern");
			jbtnOk = new StandardJButtons("Ok");
		}
		jpSouth.add(jtbtnBearbeiten);
		jpSouth.add(jbtnOk);
	}
}

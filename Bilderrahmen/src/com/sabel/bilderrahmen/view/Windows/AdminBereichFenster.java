package com.sabel.bilderrahmen.view.Windows;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.sabel.bilderrahmen.view.Buttons.StandardJButtons;

/**
 * 
 * @author Dominic Janda
 *
 */

public class AdminBereichFenster extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Container c;
	private JPanel jpcenter, jpsouth, jptext, jpButtons, jpHeader;
	private JLabel jlText, jlHeader;
	private StandardJButtons jbtnAdminbereichVerlassen, jbtnZumAdminPwBereich, jbtnZumEmailBereich;
	private ImageIcon titleImage;

	public AdminBereichFenster() {
		titleImage = new ImageIcon("PanelImages/TitleImage.png");
		this.setIconImage(titleImage.getImage());
		this.setTitle("PiLderrahmen V1.0");
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.initComponents();
		this.initEvents();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void initEvents() {
		/**
		 * Eventhandling zum Button "zurück"
		 */
		jbtnAdminbereichVerlassen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		/**
		 * Eventhandling für Button "zum AdminPasswort Bereich"
		 */
		jbtnZumAdminPwBereich.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new AdminPasswortKonfigFenster();
			}
		});

		/**
		 * Eventhandling für Button "zum Email Bereich"
		 */
		jbtnZumEmailBereich.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new EmailPasswortKonfigFenster();
			}
		});
	}

	private void initComponents() {
		c = this.getContentPane();

		/**
		 * Header Image wird erstellt
		 */
		File headerFile = new File("PanelImages/HeaderAdminbereich.png");
		BufferedImage image = null;
		try {
			image = ImageIO.read(headerFile);
		} catch (IOException e) {
			System.out.println("Headerimage nicht gefunden");
		}

		jpcenter = new JPanel();
		jpsouth = new JPanel();
		jptext = new JPanel();
		jpButtons = new JPanel();
		jpHeader = new JPanel();
		jlText = new JLabel("<html><font size=5>Wählen Sie aus, was Sie bearbeiten möchten:</font></html>");
		jlHeader = new JLabel(new ImageIcon(image));

		/**
		 * Button "Zurück" wird mit ImageIcon belegt
		 */
		try {
			BufferedImage buttonIconZurueck;
			buttonIconZurueck = ImageIO.read(new File("PanelImages/Back.png"));
			jbtnAdminbereichVerlassen = new StandardJButtons(new ImageIcon(buttonIconZurueck));
		} catch (IOException e) {
			jbtnAdminbereichVerlassen = new StandardJButtons("Zurück");
		}

		/**
		 * Buttons zur für Email-Bereich und Admin-Bereich werden angelegt
		 */
		jbtnZumAdminPwBereich = new StandardJButtons("Admin-PW ändern");
		jbtnZumEmailBereich = new StandardJButtons("E-Mail-Einstellungen ändern");

		jpHeader.add(jlHeader);
		jptext.add(jlText);
		jpButtons.add(jbtnZumAdminPwBereich);
		jpButtons.add(jbtnZumEmailBereich);
		jpsouth.add(jbtnAdminbereichVerlassen);

		jpcenter.setLayout(new BoxLayout(jpcenter, BoxLayout.Y_AXIS));

		jpcenter.add(jptext);
		jpcenter.add(jpButtons);
		c.add(jpHeader, BorderLayout.NORTH);
		c.add(jpcenter, BorderLayout.CENTER);
		c.add(jpsouth, BorderLayout.SOUTH);
	}
}

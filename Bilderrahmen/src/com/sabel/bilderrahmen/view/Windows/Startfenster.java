package com.sabel.bilderrahmen.view.Windows;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.sabel.bilderrahmen.db.Config;
import com.sabel.bilderrahmen.db.ConfigService;
import com.sabel.bilderrahmen.view.Buttons.StandardJButtons;
import com.sabel.bilderrahmen.view.Buttons.StandardJToggleButtons;

/**
 * 
 * @author Dominic Janda
 *
 */

public class Startfenster extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Container c;
	private JPanel jpSouth, jpNorth;
	private JLabel jlHeader;
	private StandardJButtons jbtnSchliessen, jbtnFertig, jbtnPasswortAendern, jbtnAbbrechen;
	private StandardJToggleButtons jtbtnBearbeiten;
	private StartfensterKonfigPanel configpanel;
	private ConfigService configservice;
	private ImageIcon titleImage;

	public Startfenster() {
		titleImage = new ImageIcon("PanelImages/TitleImage.png");
		this.setIconImage(titleImage.getImage());
		this.setTitle("PiLderrahmen V1.0");
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.initComponents();
		this.initEvents();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(false);
	}

	private void initEvents() {
		/**
		 * Eventhandling für den BeendenButton
		 */
		jbtnSchliessen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0); // Programm wird beendet
			}
		});

		/**
		 * Eventhandling für den FertigButton
		 */
		jbtnFertig.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				startfensterUnsichtbar(); // Startfenster wird unsichtbar
											// gemacht und der Bilderlauf wird
											// angezeigt
			}
		});

		/**
		 * Eventhandling für den AdminButton
		 */
		jbtnPasswortAendern.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new Loginfenster(); // Der Login zum Adminbereich wird gestartet
			}
		});

		/**
		 * Eventhandling für den Abbrechenbutton (derzeit nicht in Nutzung)
		 */
		jbtnAbbrechen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose(); // Das "Startfenster" wird geschlossen
				new Startfenster(); // Das "Startfenster" wird neu geöffnet
			}
		});

		/**
		 * Eventhandling für den BearbeitenButton
		 */
		jtbtnBearbeiten.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (jtbtnBearbeiten.isSelected()) {
					/**
					 * Der Button ist betätigt
					 */
					jtbtnBearbeiten.setIcon(new ImageIcon("PanelImages/Done.png"));
					jbtnSchliessen.setVisible(false);
					jbtnFertig.setVisible(false);
					jbtnPasswortAendern.setVisible(false);
					configpanel.aktiviereTextfelder();
				} else {
					/**
					 * Der Button wird losgelassen --> Es wird auf Fertig geklickt
					 */
					Config config = configpanel.schreibeConfig();
					if (configpanel.eingabenUeberpruefen()) {
						/**
						 * Wenn die Eingaben den Richtlinien/Vorgaben entsprechen
						 */
						configpanel.schreibeConfig();
						configservice.speichern(config);
						jtbtnBearbeiten.setIcon(new ImageIcon("PanelImages/ConfigChange.png"));
						configpanel.deaktiviereTextfelder();
						jbtnSchliessen.setVisible(true);
						jbtnFertig.setVisible(true);
						jbtnPasswortAendern.setVisible(true);
						dispose();
					}
				}
			}
		});
	}

	private void initComponents() {
		c = this.getContentPane();
		configservice = new ConfigService();
		jpSouth = new JPanel();
		jpNorth = new JPanel();

		/**
		 * Das Headerimage wird eingebunden
		 */
		File headerFile = new File("PanelImages/HeaderStartfenster.png");
		BufferedImage image = null;
		try {
			image = ImageIO.read(headerFile);
		} catch (IOException e) {
			System.out.println("Headerimage nicht gefunden");
		}

		/**
		 * Das JLabel für den Header wird erstellt und mit dem Image belegt
		 */
		jlHeader = new JLabel(new ImageIcon(image));

		/**
		 * Die einzelnen Buttons werden mit den zugehörigen Symbolen belegt, welche im Ornder "PanelImages" liegen
		 */
		try {
			BufferedImage buttonIconBearbeiten;
			buttonIconBearbeiten = ImageIO.read(new File("PanelImages/ConfigChange.png"));
			jtbtnBearbeiten = new StandardJToggleButtons(new ImageIcon(buttonIconBearbeiten));
			jtbtnBearbeiten.setToolTipText("Bearbeiten der Konfigurationseinstellungen");

			BufferedImage buttonIconFertig;
			buttonIconFertig = ImageIO.read(new File("PanelImages/Done.png"));
			jbtnFertig = new StandardJButtons(new ImageIcon(buttonIconFertig));
			jbtnFertig.setToolTipText("Konfigurationsfenster schließen und weiter mit dem Bilderlauf");

			BufferedImage buttonIconExit;
			buttonIconExit = ImageIO.read(new File("PanelImages/Exit.png"));
			jbtnSchliessen = new StandardJButtons(new ImageIcon(buttonIconExit));
			jbtnSchliessen.setToolTipText("Programm beenden");

			BufferedImage buttonIconAdmin;
			buttonIconAdmin = ImageIO.read(new File("PanelImages/Admin.png"));
			jbtnPasswortAendern = new StandardJButtons(new ImageIcon(buttonIconAdmin));
			jbtnPasswortAendern.setToolTipText("Zum Administrationsbereich");

			BufferedImage buttonIconBack;
			buttonIconBack = ImageIO.read(new File("PanelImages/Back.png"));
			jbtnAbbrechen = new StandardJButtons(new ImageIcon(buttonIconBack));
			jbtnAbbrechen.setVisible(false);
		} catch (IOException e) {
			/**
			 * Im Falle, dass eine ImageIcon für den Button nicht gefunden wird, werden die Buttons mit einer Beschriftung versehen
			 */
			jtbtnBearbeiten = new StandardJToggleButtons("Ändern");
			jbtnFertig = new StandardJButtons("Weiter");
			jbtnSchliessen = new StandardJButtons("Programm beenden");
			jbtnPasswortAendern = new StandardJButtons("Adminbereich");
			jbtnAbbrechen = new StandardJButtons("Zurück");
			jbtnAbbrechen.setVisible(false);
		}

		/**
		 * Die Buttons werden auf das JPanel South gelegt
		 */
		jpSouth.add(jbtnSchliessen);
		jpSouth.add(jtbtnBearbeiten);
		jpSouth.add(jbtnAbbrechen);
		jpSouth.add(jbtnPasswortAendern);
		jpSouth.add(jbtnFertig);

		/**
		 * Das JLabel für den Header wird auf das JPanel North gelegt
		 */
		jpNorth.add(jlHeader);

		/**
		 * Die letzte Konfiguration wird aus der Datenbank geholt und im Panel angezeigt
		 */
		configpanel = new StartfensterKonfigPanel();
		configpanel.setzeConfig(configservice.gibLetzteConfig());

		/**
		 * Die JPanels sowie das Konfigpanel werden auf die ContentPane gelegt
		 */
		c.add(jpNorth, BorderLayout.NORTH);
		c.add(jpSouth, BorderLayout.SOUTH);
		c.add(configpanel);
	}

	/**
	 * Methode um das Startfenster unsichtbar zu machen
	 */
	public void startfensterUnsichtbar() {
		this.setVisible(false);
	}


	/**
	 * Methode um den Mauszeiger mittig auf das Panel zu setzen, da der Zeiger
	 * auf dem Bilderlauf nicht angezegit wird und sonst immer gesucht werden muss
	 */
	public void mauszeigerMittigSetzen() {
		try {
			Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
			int width = (int) size.getWidth() / 2;
			int height = (int) size.getHeight() / 2;
			Robot robot = new Robot();
			robot.mouseMove(width, height);
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
}

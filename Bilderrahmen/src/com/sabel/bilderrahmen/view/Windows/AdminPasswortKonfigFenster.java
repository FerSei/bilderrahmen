package com.sabel.bilderrahmen.view.Windows;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import com.sabel.bilderrahmen.db.AdminPasswort;
import com.sabel.bilderrahmen.db.AdminPasswortService;
import com.sabel.bilderrahmen.view.Buttons.StandardJButtons;

/**
 * 
 * @author Dominic Janda
 *
 */

public class AdminPasswortKonfigFenster extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Container c;
	private StandardJButtons jbtnAbbrechen, jbtnOk;
	private JPanel jpSouth, jpHeader;
	private JLabel jlHeader;
	private AdminPasswortService adminPasswortService;
	private AdminPasswortKonfigPanel adminPasswortPanel;
	private ImageIcon titleImage;

	public AdminPasswortKonfigFenster() {
		titleImage = new ImageIcon("PanelImages/TitleImage.png");
		this.setIconImage(titleImage.getImage());
		this.setTitle("PiLderrahmen V1.0");
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.initComponents();
		this.initEvents();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void initEvents() {
		/**
		 * Eventhandling für den Button Abbrechen
		 */
		jbtnAbbrechen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

		/**
		 * Eventhandling für den Button OK
		 */
		jbtnOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				AdminPasswort adminpasswort = adminPasswortPanel.schreibeInAdminPasswortDB();
				if (adminPasswortPanel.eingabenUeberpruefen()) {
					/**
					 * Die Eingaben stimmen und werden in DB (Tabelle
					 * adminpasswort) geschrieben
					 */
					adminPasswortPanel.schreibeInAdminPasswortDB();
					adminPasswortService.speichern(adminpasswort);
					dispose();
				} else {
					/**
					 * Die Eingaben stimmen nicht
					 */
					JOptionPane.showMessageDialog(AdminPasswortKonfigFenster.this, "Bitte überprüfen Sie Ihre Eingaben",
							"Es ist ein Fehler aufgetreten", JOptionPane.WARNING_MESSAGE);
					jbtnOk.setSelected(true);
				}
			}
		});
	}

	private void initComponents() {
		c = this.getContentPane();
		adminPasswortService = new AdminPasswortService();
		adminPasswortPanel = new AdminPasswortKonfigPanel();

		/**
		 * Header Image wird erstellt
		 */
		File headerFile = new File("PanelImages/HeaderAdminPasswort.png");
		BufferedImage image = null;
		try {
			image = ImageIO.read(headerFile);
		} catch (IOException e) {
			System.out.println("Headerimage nicht gefunden");
		}

		jpSouth = new JPanel();
		jpHeader = new JPanel();
		jlHeader = new JLabel(new ImageIcon(image));
		jpHeader.add(jlHeader);

		/**
		 * Die ImageIcons werden auf die Buttons gelegt
		 */
		try {
			BufferedImage buttonIconSpeichern;
			buttonIconSpeichern = ImageIO.read(new File("PanelImages/Done.png"));
			jbtnOk = new StandardJButtons(new ImageIcon(buttonIconSpeichern));

			BufferedImage buttonIconBack;
			buttonIconBack = ImageIO.read(new File("PanelImages/Back.png"));
			jbtnAbbrechen = new StandardJButtons(new ImageIcon(buttonIconBack));
		} catch (IOException e) {
			jbtnOk = new StandardJButtons("Ändern");
			jbtnAbbrechen = new StandardJButtons("Ok");
		}

		jpSouth.add(jbtnOk);
		jpSouth.add(jbtnAbbrechen);

		c.add(jpHeader, BorderLayout.NORTH);
		c.add(adminPasswortPanel);
		c.add(jpSouth, BorderLayout.SOUTH);
	}
}

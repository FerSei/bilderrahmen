package com.sabel.bilderrahmen.view.Windows;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.KeyStroke;

import com.sabel.bilderrahmen.db.AdminPasswort;
import com.sabel.bilderrahmen.db.AdminPasswortService;
import com.sabel.bilderrahmen.view.Buttons.StandardJButtons;

/**
 * 
 * @author Dominic Janda
 *
 */

public class Loginfenster extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9180598133072828589L;
	private Container c;
	private JPanel jpCenter, jpSouth, jpNorth, jpLabel, jpJPass;
	private StandardJButtons jbtnZurueck, jbtnOk;
	private JLabel jlInfoText, jlHeader;
	private JPasswordField jpassPasswort;
	private AdminPasswortService adminPasswortService;
	private AdminPasswort adminPasswort;
	private ImageIcon titleImage;

	public Loginfenster() {
		titleImage = new ImageIcon("PanelImages/TitleImage.png");
		this.setIconImage(titleImage.getImage());
		this.setTitle("PiLderrahmen V1.0");
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.initComponents();
		this.initEvents();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	private void initEvents() {
		/**
		 * Eventhandling für JButton OK (Login wird durchgeführt nach
		 * erfolgreicher Prüfung
		 */
		jbtnOk.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				loginAusfuehren();
			}
		});

		/**
		 * Wenn die Taste "Enter" gedrückt wird, wird der Login durchgeführt,
		 * sofern die Prüfung erfolgreich verlief
		 */
		jpCenter.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"forward");
		jpCenter.getActionMap().put("forward", new AbstractAction() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				loginAusfuehren();
			}
		});

		/**
		 * Eventhandling für JButton abbrechen. Fenster wird geschlossen -->
		 * Zurück zu Startfenster
		 */
		jbtnZurueck.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void initComponents() {
		c = this.getContentPane();

		jpNorth = new JPanel();
		jpCenter = new JPanel();
		jpSouth = new JPanel();
		jpJPass = new JPanel();
		jpLabel = new JPanel();

		/**
		 * Das Headerimage wird eingebunden
		 */
		File headerFile = new File("PanelImages/HeaderLogin.png");
		BufferedImage image = null;
		try {
			image = ImageIO.read(headerFile);
		} catch (IOException e) {
			System.out.println("Headerimage nicht gefunden");
		}

		/**
		 * JLabel und JPasswordField wird erstellt
		 */
		jlHeader = new JLabel(new ImageIcon(image));
		jlInfoText = new JLabel("<html><font size=5>Bitte geben Sie das Administratior-Passwort ein:</font></html>");
		jpassPasswort = new JPasswordField(15);

		/**
		 * Die ButtonIcons werden auf die Buttons gesetzt
		 */
		try {
			BufferedImage buttonIconZurueck;
			buttonIconZurueck = ImageIO.read(new File("PanelImages/Back.png"));
			jbtnZurueck = new StandardJButtons(new ImageIcon(buttonIconZurueck));

			BufferedImage buttonIconFertig;
			buttonIconFertig = ImageIO.read(new File("PanelImages/Done.png"));
			jbtnOk = new StandardJButtons(new ImageIcon(buttonIconFertig));

		} catch (IOException e) {
			/**
			 * Falls die Icons nicht gefunden werden
			 */
			jbtnOk = new StandardJButtons("Ok");
			jbtnZurueck = new StandardJButtons("Zurück");
		}

		/**
		 * Verbindung zur DB wird aufgebaut
		 */
		adminPasswort = new AdminPasswort();
		adminPasswortService = new AdminPasswortService();

		jpNorth.setLayout(new FlowLayout(FlowLayout.CENTER));
		jpJPass.setLayout(new FlowLayout(FlowLayout.CENTER));
		jpLabel.setLayout(new FlowLayout(FlowLayout.CENTER));
		jpSouth.setLayout(new FlowLayout(FlowLayout.CENTER));
		jpCenter.setLayout(new BoxLayout(jpCenter, BoxLayout.Y_AXIS));

		jpNorth.add(jlHeader);

		jpLabel.add(jlInfoText);
		jpJPass.add(jpassPasswort);
		jpCenter.add(jpLabel);
		jpCenter.add(jpJPass);

		jpSouth.add(jbtnZurueck);
		jpSouth.add(jbtnOk);

		c.add(jpNorth, BorderLayout.NORTH);
		c.add(jpCenter, BorderLayout.CENTER);
		c.add(jpSouth, BorderLayout.SOUTH);

	}

	/**
	 * Methode in welcher geprüft wird, ob das eingegebene Passwort stimmt.
	 * 
	 * @return boolean true = Passwort ist korrekt, false = Passwort ist falsch
	 */
	public boolean eingabenUeberpruefen() {
		adminPasswort = adminPasswortService.gibLetztesAdminPasswort();
		String passwortEingabe = new String(this.jpassPasswort.getPassword()).trim();
		StringBuffer sb = new StringBuffer();

		/**
		 * Aus der Eingabe wird ein Hash per MD5 gebildet und mit dem Inhalt der
		 * DB Adminpasswort verglichen
		 */
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(passwortEingabe.getBytes());
			byte[] digest = md.digest();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}

		} catch (NoSuchAlgorithmException e) {
			JOptionPane.showMessageDialog(Loginfenster.this, "Anmeldung im Moment nicht möglich",
					"Der MD5-Algorithmus ist nicht vorhanden", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		/**
		 * Der Vergleich findet statt
		 */
		if (sb.toString().equals(adminPasswort.getAdminPasswort())) {
			/**
			 * Eingabe und DB Inhalt stimmen überein
			 */
			return true;
		}
		/**
		 * Die Eingabe ist falsch
		 */
		return false;
	}

	/**
	 * Methode um den Login druchzuführen
	 */
	public void loginAusfuehren() {
		if (eingabenUeberpruefen()) {
			/**
			 * Methode eingabenUeberpruefen hat True zurückgegeben (Passwort
			 * stimmt)
			 */
			new AdminBereichFenster();
			dispose();
		} else {
			/**
			 * Methode eingabenUeberpruefen hat False zurückgegeben (Passwort
			 * stimmt nicht)
			 */
			JOptionPane.showMessageDialog(Loginfenster.this, "Falsches Passwort", "Das Passwort ist falsch",
					JOptionPane.ERROR_MESSAGE);
		}
	}
}

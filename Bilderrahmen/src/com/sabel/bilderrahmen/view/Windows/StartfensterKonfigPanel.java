package com.sabel.bilderrahmen.view.Windows;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.sabel.bilderrahmen.db.Config;

/**
 * 
 * @author Dominic Janda
 *
 */

public class StartfensterKonfigPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel[] jpanels;
	private JLabel[] jlabels;
	private JTextField[] jtxtfields;

	public StartfensterKonfigPanel() {
		this.initComponents();
		this.deaktiviereTextfelder();
	}

	private void initComponents() {
		jpanels = new JPanel[4];
		jlabels = new JLabel[4];
		jtxtfields = new JTextField[4];

		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		for (int i = 0; i < 4; i++) {
			jpanels[i] = new JPanel();
			jpanels[i].setLayout(new FlowLayout(FlowLayout.RIGHT));
			jlabels[i] = new JLabel();
			jtxtfields[i] = new JTextField(20);
			jpanels[i].add(jlabels[i]);
			jpanels[i].add(jtxtfields[i]);
			this.add(jpanels[i]);
		}

		jlabels[0].setText("Gerätename:");
		jlabels[1].setText("Anzahl angezeigter Bilder:");
		jlabels[2].setText("Dauer der Anzeige (Sek.):");
		jlabels[3].setText("neue Emails prüfen (Min.):");
	}

	/**
	 * Letzte Config wird aus DB ausgelesen und im Panel angezeigt
	 * 
	 * @param config
	 */
	public void setzeConfig(Config config) {
		/**
		 * Umrechnung von Millisekunden in Sekunden (Anzeige in Sekunden,
		 * Speicherung in DB in Millisekunden) Faktor 1000
		 */
		int anzeigeDauer = config.getAnzeigedauer() / 1000;
		/**
		 * Umrechnungn von Millisekunden in Minuten (Anzeig in Minuten,
		 * Speicherung in Millisekunden) Faktor 60000
		 */
		int ladeZeit = config.getLadezeit() / 60000;

		jtxtfields[0].setText(config.getGeraetename());
		jtxtfields[1].setText(Integer.toString(config.getAnzahlBilder()));
		jtxtfields[2].setText(Integer.toString(anzeigeDauer));
		jtxtfields[3].setText(Integer.toString(ladeZeit));
	}

	public Config schreibeConfig() {
		Config config = new Config();
		if (!jtxtfields[1].getText().isEmpty()) {
			config.setAnzahlBilder(Integer.parseInt(jtxtfields[1].getText()));
		}
		if (!jtxtfields[2].getText().isEmpty()) {
			/**
			 * Umrechnung von Sekunden in Millisekunden
			 */
			int anzeigeDauer = Integer.parseInt(jtxtfields[2].getText()) * 1000;
			config.setAnzeigedauer(anzeigeDauer);
		}
		if (!jtxtfields[3].getText().isEmpty()) {
			/**
			 * Umrechnung von Minuten in Millisekunden
			 */
			int ladeZeit = Integer.parseInt(jtxtfields[3].getText()) * 60000;
			config.setLadezeit(ladeZeit);
		}
		config.setGeraetename(jtxtfields[0].getText());
		config.setDatum(config.aktuellesDatum());
		return config;
	}

	public void aktiviereTextfelder() {
		for (int i = 0; i < 4; i++) {
			jtxtfields[i].setEnabled(true);
		}
	}

	public void deaktiviereTextfelder() {
		for (int i = 0; i < 4; i++) {
			jtxtfields[i].setEnabled(false);
		}
	}

	public boolean eingabenUeberpruefen() {
		/**
		 * Farbe für Fehlermeldungen werden hier definiert
		 */
		Color leichtesRot = new Color(252, 189, 189);
		Color rot = new Color(255, 0, 0);

		/**
		 * Prüfung, ob die Eingabe bei Gerätename leer ist
		 */
		String gereatename = this.jtxtfields[0].getText().trim();
		if (gereatename.isEmpty()) {
			JOptionPane.showMessageDialog(StartfensterKonfigPanel.this, "Bitte geben Sie einen Gerätenamen an",
					"Fehler", JOptionPane.ERROR_MESSAGE);
			jtxtfields[0].setBackground(leichtesRot);
			jlabels[0].setForeground(rot);
			return false;
		}

		/**
		 * Prüfung, ob die Eingabe der Anzahl der angezeigten Bilder leer ist
		 */
		if (jtxtfields[1].getText().isEmpty()) {
			JOptionPane.showMessageDialog(StartfensterKonfigPanel.this,
					"Sie müssen die Anzahl der Bilder angeben, wieviele angezeigt werden sollen", "Fehler",
					JOptionPane.ERROR_MESSAGE);
			jtxtfields[1].setBackground(leichtesRot);
			jlabels[1].setForeground(rot);
			return false;
		} else {
			/**
			 * Prüfung, ob der Wert der Anzahl der angezeigten Bilder kleiner 2
			 * ist
			 */
			int anzahlAngezeigteBilder = Integer.parseInt(jtxtfields[1].getText());
			if (anzahlAngezeigteBilder < 2) {
				JOptionPane.showMessageDialog(StartfensterKonfigPanel.this,
						"Es müssen mindestens 2 Bilder angezeigt werden", "Fehler", JOptionPane.ERROR_MESSAGE);
				jtxtfields[1].setBackground(leichtesRot);
				jlabels[1].setForeground(rot);
				return false;
			}
		}

		/**
		 * Prüfung, ob die Eingabe der Dauer der Anzeige pro Bild leer ist
		 */
		if (jtxtfields[2].getText().isEmpty()) {
			JOptionPane.showMessageDialog(StartfensterKonfigPanel.this,
					"Sie müssen angeben, wie lange ein Bild angezeigt werden soll", "Fehler",
					JOptionPane.ERROR_MESSAGE);
			jtxtfields[2].setBackground(leichtesRot);
			jlabels[2].setForeground(rot);
			return false;
		} else {
			/**
			 * Prüfung, ob die Eingabe der Dauer der Anzeige pro Bild kleiner 2
			 * oder größer 60 ist
			 */
			int dauerAnzeigeBild = Integer.parseInt(jtxtfields[2].getText());
			if (dauerAnzeigeBild < 2 || dauerAnzeigeBild > 60) {
				JOptionPane.showMessageDialog(StartfensterKonfigPanel.this,
						"Geben Sie einen Wert für die Anzeigedauer pro Bild zwischen 2 und 60 Sekunden ein.", "Fehler",
						JOptionPane.ERROR_MESSAGE);
				jtxtfields[2].setBackground(leichtesRot);
				jlabels[2].setForeground(rot);
				return false;
			}
		}

		/**
		 * Prüfung, ob die Eingabe der Zeitspanne zwischen dem erneuten Laden
		 * der Konfiguration leer ist
		 */
		if (jtxtfields[3].getText().isEmpty()) {
			JOptionPane.showMessageDialog(StartfensterKonfigPanel.this,
					"Sie müssen angeben, wie lange die Zeitspanne bis zum erneuten Laden der Konfiguration ist",
					"Fehler", JOptionPane.ERROR_MESSAGE);
			jtxtfields[3].setBackground(leichtesRot);
			jlabels[3].setForeground(rot);
			return false;
		} else {
			/**
			 * Prüfung, ob die Eingabe der Zeitspanne zwischen dem erneuten
			 * Laden der Konfiguration kleienr 5 Minuten ist
			 */
			int updateIntervall = Integer.parseInt(jtxtfields[3].getText());
			if (updateIntervall < 0 || updateIntervall > 180) {
				JOptionPane.showMessageDialog(StartfensterKonfigPanel.this,
						"Sie müssen angeben in welchen Abständen nach geänderten Konfigurationen gesucht werden soll. Geben Sie einen Wert zwischen 5 und 180 Minuten an.",
						"Fehler", JOptionPane.ERROR_MESSAGE);
				jtxtfields[3].setBackground(leichtesRot);
				jlabels[3].setForeground(rot);
				return false;
			}
		}

		for (int i = 0; i < 4; i++) {
			/**
			 * JLabels werden wieder mit schwarzer Schrift dargestellt
			 */
			jlabels[i].setForeground(Color.BLACK);
			/**
			 * JTextFields werden wieder auf Default gesetzt (Roter Hintergrund
			 * wird entfernt)
			 */
			jtxtfields[i].setOpaque(false);
		}
		return true;
	}
}
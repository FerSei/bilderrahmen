package com.sabel.bilderrahmen.view.Windows;

import java.awt.FlowLayout;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import com.sabel.bilderrahmen.db.AdminPasswort;

/**
 * 
 * @author Dominic Janda
 *
 */

public class AdminPasswortKonfigPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel jpNeuesPw, jpNeuesPwWiederholen, jpButton;
	private JLabel jlNeuesPw, jlNeuesPwWiederholen;
	private JPasswordField jpassNeu, jpassNeuWiederholen;

	public AdminPasswortKonfigPanel() {
		this.initComponents();
	}

	private void initComponents() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		jpNeuesPw = new JPanel();
		jpNeuesPwWiederholen = new JPanel();
		jpButton = new JPanel();

		jlNeuesPw = new JLabel("neues Admin Passwort:");
		jlNeuesPwWiederholen = new JLabel("Wiederholen:");
		jpassNeu = new JPasswordField(20);
		jpassNeuWiederholen = new JPasswordField(20);

		jpNeuesPw.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jpNeuesPwWiederholen.setLayout(new FlowLayout(FlowLayout.RIGHT));
		jpButton.setLayout(new FlowLayout(FlowLayout.CENTER));

		jpNeuesPw.add(jlNeuesPw);
		jpNeuesPw.add(jpassNeu);
		jpNeuesPwWiederholen.add(jlNeuesPwWiederholen);
		jpNeuesPwWiederholen.add(jpassNeuWiederholen);

		this.add(jpNeuesPw);
		this.add(jpNeuesPwWiederholen);
		this.add(jpButton);
	}

	/**
	 * Methode um das Passwort in die DB zu schreiben
	 * 
	 * @return AdminPasswort
	 */
	public AdminPasswort schreibeInAdminPasswortDB() {
		AdminPasswort adminPasswort = new AdminPasswort();
		String passwortEingabe = new String(this.jpassNeu.getPassword()).trim();
		StringBuffer sb = new StringBuffer();
		try {
			/**
			 * MD5 Hash des Passwortes wird erzeugt
			 */
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(passwortEingabe.getBytes());
			byte[] digest = md.digest();
			for (byte b : digest) {
				sb.append(String.format("%02x", b & 0xff));
			}
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Algorithymus nicht vorhanden");
			return adminPasswort;
		}
		adminPasswort.setAdminPasswort(sb.toString());
		return adminPasswort;
	}

	/**
	 * Methode, um die Eingaben zu überprüfen
	 * 
	 * @return boolean true = Eingaben sind korrekt, false = Eingaben sind
	 *         falsch
	 */
	public boolean eingabenUeberpruefen() {
		String passwort = new String(this.jpassNeu.getPassword()).trim();
		String passwortWiederholung = new String(this.jpassNeuWiederholen.getPassword()).trim();
		if (passwort.equals(passwortWiederholung) && !passwort.isEmpty()) {
			/**
			 * Es wird geprüft, ob dass Passwort: - nicht leer ist - ob beide
			 * Passwörter übereinstimmen
			 */
			JOptionPane.showMessageDialog(AdminPasswortKonfigPanel.this, "Die Änderungen wurden gespeichert", "Erfolg",
					JOptionPane.INFORMATION_MESSAGE);
			return true;
		}
		return false;
	}
}

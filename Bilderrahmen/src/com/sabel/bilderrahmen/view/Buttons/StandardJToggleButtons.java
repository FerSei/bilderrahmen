package com.sabel.bilderrahmen.view.Buttons;

import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

/**
 * 
 * @author Dominic Janda
 *
 */

public class StandardJToggleButtons extends JToggleButton {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Color obenColorLeichtRot = new Color(255, 90, 90);
	private Color untenColorRot = new Color(255, 120, 120);
	private Color hoverColor = new Color(138, 138, 138);
	private Color gedruecktColor = new Color(180, 0, 0);;
	private int aussenRundung = 10;
	private int innenRundung = 8;
	private GradientPaint gradientPaint;

	public StandardJToggleButtons(String text) {
		super();
		setText(text);
		setContentAreaFilled(false);
		setBorderPainted(false);
		setFont(new Font("Verdana", Font.BOLD, 12));
		setForeground(Color.WHITE);
		setFocusable(false);
	}

	public StandardJToggleButtons(ImageIcon imageIcon) {
		super();
		setIcon(imageIcon);
		setContentAreaFilled(false);
		setBorderPainted(false);
		setFocusable(false);
	}

	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		int h = getHeight();
		int w = getWidth();
		ButtonModel model = getModel();
		if (!model.isEnabled()) {
			setForeground(Color.GRAY);
			gradientPaint = new GradientPaint(0, 0, new Color(192, 192, 192), 0, h, new Color(192, 192, 192), true);
		} else {
			setForeground(Color.WHITE);
			if (model.isRollover()) {
				gradientPaint = new GradientPaint(0, 0, hoverColor, 0, h, hoverColor, true);
			} else {
				gradientPaint = new GradientPaint(0, 0, obenColorLeichtRot, 0, h, untenColorRot, true);
			}
		}
		g2d.setPaint(gradientPaint);
		GradientPaint p1;
		GradientPaint p2;
		if (model.isPressed()) {
			gradientPaint = new GradientPaint(0, 0, gedruecktColor, 0, h, gedruecktColor, true);
			g2d.setPaint(gradientPaint);
			p1 = new GradientPaint(0, 0, new Color(0, 0, 0), 0, h - 1, new Color(100, 100, 100));
			p2 = new GradientPaint(0, 1, new Color(0, 0, 0, 50), 0, h - 3, new Color(255, 255, 255, 100));
		} else {
			p1 = new GradientPaint(0, 0, new Color(100, 100, 100), 0, h - 1, new Color(0, 0, 0));
			p2 = new GradientPaint(0, 1, new Color(255, 255, 255, 100), 0, h - 3, new Color(0, 0, 0, 50));
			gradientPaint = new GradientPaint(0, 0, obenColorLeichtRot, 0, h, untenColorRot, true);
		}
		RoundRectangle2D.Float r2d = new RoundRectangle2D.Float(0, 0, w - 1, h - 1, aussenRundung, aussenRundung);
		Shape clip = g2d.getClip();
		g2d.clip(r2d);
		g2d.fillRect(0, 0, w, h);
		g2d.setClip(clip);
		g2d.setPaint(p1);
		g2d.drawRoundRect(0, 0, w - 1, h - 1, aussenRundung, aussenRundung);
		g2d.setPaint(p2);
		g2d.drawRoundRect(1, 1, w - 3, h - 3, innenRundung, innenRundung);
		g2d.dispose();

		super.paintComponent(g);
	}

	/**
	 * Methode zum setzen der oberen Farbe des Buttons
	 * 
	 * @param color
	 */
	public void setStartColor(Color color) {
		obenColorLeichtRot = color;
	}

	/**
	 * Methode zum setzen der unteren Farbe des Buttons
	 * 
	 * @param pressedColor
	 */
	public void setEndColor(Color pressedColor) {
		untenColorRot = pressedColor;
	}

	/**
	 * Methode, welche die obere Farbe des Buttons zurückgibt
	 * 
	 * @return Obere Farbe des Buttons
	 */
	public Color getStartColor() {
		return obenColorLeichtRot;
	}

	/**
	 * Methode, welche die untere Farbe des Buttons zurückgibt
	 * 
	 * @return Untere Farbe des Buttons
	 */
	public Color getEndColor() {
		return untenColorRot;
	}
}
